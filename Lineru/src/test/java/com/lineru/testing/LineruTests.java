package com.lineru.testing;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Beneficio_Test.class, CalcularCredito_Test.class, CrearCuenta_Test.class })
public class LineruTests {

}
