package com.lineru.testing;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.lineru.testing.pages.CalcularCreditoPage;

public class CalcularCredito_Test {
	
	private WebDriver driver;
	CalcularCreditoPage calcularCreditoPage;

	@Before
	public void setUp() throws Exception {
		calcularCreditoPage = new CalcularCreditoPage(driver);
		driver = calcularCreditoPage.chromeDriverConexion();
		calcularCreditoPage.abrir("https://www.lineru.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
	}

	@After
	public void tearDown() throws Exception {
		driver.close();
	}

	@Test
	public void test() throws InterruptedException {
		calcularCreditoPage.CalcularCredito();
	}

}
