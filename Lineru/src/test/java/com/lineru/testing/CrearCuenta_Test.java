package com.lineru.testing;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.lineru.testing.pages.CrearCuentaPage;

public class CrearCuenta_Test {
	
	private WebDriver driver;
	CrearCuentaPage crearCuentaPage;

	@Before
	public void setUp() throws Exception {
		crearCuentaPage = new CrearCuentaPage(driver);
		driver = crearCuentaPage.chromeDriverConexion();
		crearCuentaPage.abrir("https://www.lineru.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.close();
	}

	@Test
	public void test() throws InterruptedException {
		crearCuentaPage.CrearCuenta();
		Thread.sleep(2000);
		assertTrue(crearCuentaPage.faltaAceptarisplayed());
	}

}
