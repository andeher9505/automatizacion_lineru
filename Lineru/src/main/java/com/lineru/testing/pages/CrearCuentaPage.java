package com.lineru.testing.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CrearCuentaPage extends HomeLineruPOM {

	By clickMiCuenta = By.xpath("//header/div[1]/div[1]/div[1]/nav[1]/a[1]");
	//By Inicia = By.xpath("//h3[contains(text(),'Inicia sesi�n en Lineru')]");
	By Creala = By.xpath("/html/body/app-root/app-auth-login/auth-header/header/div[2]/div/button");
	By PrimerNombre = By.xpath("//input[@id='mat-input-10']");
	By SeleccTipoDoc = By.xpath("//span[contains(text(),'Selecciona una opci�n')]");
	By NumDocumento = By.xpath("//*[@id=\"mat-input-11\"]");
	By NumCelular = By.xpath("//*[@id=\"mat-input-12\"]");
	By Correo = By.xpath("//*[@id=\"mat-input-13\"]");
	By Clave = By.xpath("//*[@id=\"mat-input-14\"]");
	By AcepTerCond = By.xpath("/html/body/app-root/app-auth-register/div/div[2]/div/div/div[1]/div/form/div[2]/div/app-checkbox[1]/div/div/div/label");
	By EmpezarYa = By.xpath("/html/body/app-root/app-auth-register/div/div[2]/div/div/div[1]/div/form/div[3]/button");
	By FaltaAcept = By.xpath("//span[contains(text(),'Debes aceptar para continuar')]");
	
	
	public CrearCuentaPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void CrearCuenta () throws InterruptedException {
		click(clickMiCuenta);
		Thread.sleep(2000);
		if (isSelected(Creala)) {
			click(Creala);
			escribir("Andres QA", PrimerNombre);
			click(SeleccTipoDoc);
			click(NumDocumento);
			escribir("3112817473", NumCelular);
			escribir("zinobe@gmail.com", Correo);
			escribir("admin123", Clave);
			click(AcepTerCond);
			click(EmpezarYa);
		} else {
			System.out.println("Campos no presentes en la pagina");

		}
	}
	
	public boolean faltaAceptarisplayed () {
		return isSelected(FaltaAcept);
	}

}
