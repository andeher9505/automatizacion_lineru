package com.lineru.testing.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BeneficioPage extends HomeLineruPOM {

	By clickBeneiciosLocalizador = By.xpath("//span[contains(text(),'Beneficios')]");
	By acumularLocalizador = By.xpath("//h3[contains(text(),'ACUMULA PUNTOS, DESBLOQUEA NIVELES Y CANJ�ALOS POR')]");
	By clickLinerLocalizador = By.xpath("//span[contains(text(),'acerca')]");
	By clicknivelLocalizador = By.xpath("//*[@id=\"carousel\"]/div[1]/div/h2");
	By clickNivelSeisLocalizador = By.xpath("//*[@id=\"ngb-slide-38\"]");
	
	public BeneficioPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void BeneficioCliente () throws InterruptedException {
		
		click(clickBeneiciosLocalizador);
		Thread.sleep(2000);
		
		if (isSelected(clickLinerLocalizador)) {
			click(clickLinerLocalizador);
			Thread.sleep(3000);
			click(clickNivelSeisLocalizador);
			Thread.sleep(1000);
		} else {
			System.out.println("Pagina de beneficios no encontrada");

		}

}


}
