package com.lineru.testing.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CalcularCreditoPage extends HomeLineruPOM {
	
	private WebDriver driver;
	
	By IngresarValor = By.xpath("//*[@id=\"mat-input-0\"]");
	By ObtenerMensajeAlerta = By.xpath("//div[contains(text(),'S� es tu primera vez, s�lo puedes solicitar hasta ')]");
	By ObtenerValor = By.xpath("//strong[contains(text(),'$920,000')]");
	
	
	public void CalcularCredito () throws InterruptedException {
		borrar(IngresarValor);
		Thread.sleep(1000);
		escribir("920000", IngresarValor);
		if (isSelected(ObtenerMensajeAlerta)) {
			click(ObtenerValor);
		} else {
			System.out.println("Mensaje de alerta no encontrado");

		}

	}
	
	public boolean MensajeAlertaisDisplayed () {
		return isSelected(ObtenerMensajeAlerta);
		
	}

	public CalcularCreditoPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

}
