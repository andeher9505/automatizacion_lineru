package com.lineru.testing.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HomeLineruPOM {

	private WebDriver driver;
	
	public HomeLineruPOM (WebDriver driver) {
		this.driver = driver;
		
	}
	
	public WebDriver chromeDriverConexion () {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Andr�s Felipe Pulido\\eclipse-workspace\\Lineru\\chromedriver.exe");
		driver = new ChromeDriver();
		return driver;
		
	}
	
	public WebElement findElement (By localizador) {
		return driver.findElement(localizador);
		
	}
	
	public List<WebElement> findElements (By localizador){
		return driver.findElements(localizador);
		
	}
	
	public String getText (WebElement element) {
		return element.getText();
		
	}
	
	public String getText (By localizador) {
		return driver.findElement(localizador).getText();
		
	}
	
	public void escribir(String inpuText, By localizador) {
		driver.findElement(localizador).sendKeys(inpuText);

	}
	
	public void borrar (By localizador) {
		driver.findElement(localizador).clear();
	}
	
	public void click (By localizador) {
		driver.findElement(localizador).click();
		
	}
	
	public Boolean isSelected (By localizador) {
		try {
			return driver.findElement(localizador).isSelected();
		} catch (org.openqa.selenium.NoSuchElementException e) {
			return false;
		}
	}
	
	public void abrir (String url) {
		driver.get(url);
		
	}
}
