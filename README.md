La estructura del proyecto es la siguiente usando el patron de diseño POM page object model:
1. En el paquete com.lineru.testing.pages se encuetran las clases que son las paginas (pages) que se automatizaron, la clase HomeLineruPOM es la base que permite aislar las clases pages de los comandos de selenium. Las clases (Page Objects) BeneficioPage, CalcularCreditoPage y CrearCuetaPage heredan la clase HomeLineruPOM y centralizan los localizadores y acciones de la paginas
2. El paquete com.lineru.testing son los JUnits (Tests) que usan las clases Page Objects Beneficio_Test, CalcularCreditoTest, CrearCuetaTest. 
3. Dentro del paquete com.lineru.testing se encuetra el test suite que ejecutan toda la automatizacion

Para ejecutar la automatizacion es necesario ingresar a la clase HomeLineruPOM y modificar la ruta del chromedriver. Y por último desde el test suite se ejecutan la automatizacion dando clic derecho, seleccionar Run As Junit Test.
